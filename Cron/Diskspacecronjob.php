<?php
/**
 * Copyright © 2015 Customer Paradigm. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace CustomerParadigm\DiskSpaceReport\Cron;
 
class Diskspacecronjob
{
    /** @var \CustomerParadigm\DiskSpaceReport\Model\CronFactory $cronFactory */
    protected $cronFactory;
    /** @var \CustomerParadigm\DiskSpaceReport\Model\EmailFactory $emailFactory */
    protected $emailFactory;
    /** @var \Magento\Framework\Stdlib\DateTime\DateTime $date */
    protected $date;
    /** @var \Magento\Framework\App\Filesystem\DirectoryList $directoryList */
    protected $directoryList;

    protected $cronModel;

    protected $topPercent;

    protected $logger;
 
    /**
     * @param \CustomerParadigm\DiskSpaceReport\Model\CronFactory $cronFactory
     * @param \CustomerParadigm\DiskSpaceReport\Model\EmailFactory $emailFactory
     * @param \Magento\Framework\Stdlib\DateTime\DateTime
     */	
    public function __construct(
        \CustomerParadigm\DiskSpaceReport\Model\CronFactory $cronFactory,
        \CustomerParadigm\DiskSpaceReport\Model\EmailFactory $emailFactory,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->cronFactory = $cronFactory;
        $this->emailFactory = $emailFactory;
        $this->date = $date;
        $this->logger = $logger;
    }

    /**
     * runs git status check via CRON
     *
     * @return void
     */
    public function runCheck() {
        // set max execution time for this script only
        ini_set('max_execution_time', 180); // set to 3 minutes max
        /** @var timestamp $timestamp */
        $timestamp = date('Y-m-d G:i:s');
        /** @var int $mode */
        $mode = 1;
        /** @var \Magento\Framework\Stdlib\DateTime\DateTime */
        $date = $this->date->gmtDate();
        // format date
        $date = explode(' ', date('i G j n w', strtotime($date)));

        /** @var \CustomerParadigm\DiskSpaceReport\Model\Resource\Cron\Collection $cronCollection */
        $cronCollection = $this->cronFactory->create()->getCollection();
        /** @var \CustomerParadigm\DiskSpaceReport\Model\CronFactory $cronModel */
        $cronModel = $cronCollection->getFirstItem();

        $this->cronModel = $cronModel;

        // verify CRON is enabled and if CRON job needs to be run
        if ($cronModel->getData('status')) {
            if ($cronModel->getDweek() != 99) {
                if ($cronModel->getDweek() != $date[4]) {
                    return;
                }
            }
            if ($cronModel->getMonth() != 99) {
                if ($cronModel->getMonth() != $date[3]) {
                    return;
                }
            }
            if ($cronModel->getDmonth() != 99) {
                if ($cronModel->getDmonth() != $date[2]) {
                    return;
                }
            }
            if ($cronModel->getHour() != 99) {
                if ($cronModel->getHour() != $date[1]) {
                    return;
                }
            }
            $minute = $cronModel->getData('minute');
            if ($minute != 99) {
                if ((abs($minute - ($date[0]))) > 7) {
                    return;
                }
            }
        } else {
            // disabled - do not execute
            return;
        }
        /** @var string $fromAddress */
        $from_address = '';
        /** @var string $addresses */
        $addresses = '';
        /** @var string $subject */
        $subject = '';
        /** @var string $message */
        $message = "<strong>Magento 2 Disk Space Report:</strong></br>";
        /** @var int $send_mail */
        $send_email = 0;
        /** @var \CustomerParadigm\DiskSpaceReport\Model\ResourceModel\Email\Collection $emailCollection */
        $emailCollection = $this->emailFactory->create()->getCollection();
        /** @var \CustomerParadigm\DiskSpaceReport\Model\EmailFactory $emailModel */
        if ($emailModel = $emailCollection->getFirstItem()) {
            $from_address = $emailModel->getData('from_address');
            $addresses = $emailModel->getData('addresses');
            $subject = $emailModel->getData('subject');
            $sendEmailSetting = $emailModel->getData('send_email');
        }
        // is shell code enabled on server
        if (! $this->checkExec()) {
            $message = 'The script is not allowed to run due to your server settings preventing shell commands.';
            $this->sendMessage($from_address, $addresses, $subject, $message);
            return;
        }
        // is df installed?
        if (! $this->checkDf()) {
            $message = 'Df command is not currently installed or available.';
            $this->sendMessage($from_address, $addresses, $subject, $message);
            return;
        }

        $resArray = $this->getDiskSpace();
        $topPercent = $resArray['topPercent'];
        $message .= $resArray['tableString'];

        // DEBUGGING *********************************************
        $this->logger->debug($sendEmailSetting);
        $this->logger->debug($topPercent);
        // DEBUGGING *********************************************

        if(
            $sendEmailSetting == '0' ||
            ($sendEmailSetting == '1' && $topPercent >= 60) ||
            ($sendEmailSetting == '2' && $topPercent >= 65) ||
            ($sendEmailSetting == '3' && $topPercent >= 70) ||
            ($sendEmailSetting == '4' && $topPercent >= 75) ||
            ($sendEmailSetting == '5' && $topPercent >= 80) ||
            ($sendEmailSetting == '6' && $topPercent >= 85) ||
            ($sendEmailSetting == '7' && $topPercent >= 90) ||
            ($sendEmailSetting == '8' && $topPercent >= 95) ||
            ($sendEmailSetting == '9' && $topPercent >= 99)
        ) {
            $this->sendMessage($from_address, $addresses, $subject, $message);
        }
    }

    /**
     * check to see if shell_exec is allowed
     *
     * @return boolean
     */
    public function checkExec()
    {
        return is_callable('shell_exec') && false === stripos(ini_get('disable_functions'), 'shell_exec');
    }

    /**
     * is GIT installed
     * 
     * @return boolean
     */
    public function checkDf()
    {
        /** @var string $command */
        $command = 'which df';
        /** @var string $output */
        $output = shell_exec($command);
        if ($output == '') {
            return false;
        }
        return true;
    }

    /**
     * run GIT status and return output
     *
     * @return string
     */
    protected function getDiskSpace()
    {
        $dfCommand = 'df --output=source,used,avail,pcent -h';
        $dfCommandResult = shell_exec($dfCommand); // executing command
        $arExplodeResult = explode(PHP_EOL, $dfCommandResult); // exploding result by new line character and creating array

        // Going through each line of the command result
        // Key has line index and value has the line itself
        foreach($arExplodeResult as $key => $value) {
            if($value != '') {
                $noDoubleSpaceString = preg_replace('/\s+/', ' ', $value); // removing all repeating space chars

                if($noDoubleSpaceString && $key != 0) {
                    $arDfCommandResultNoDoubleSpaces[] = explode(' ', $noDoubleSpaceString); // exploding by space character
                } else {
                    $arHeaderResultNoDoubleSpaces = explode(' ', $noDoubleSpaceString); // exploding by space character
                }
            }
        }

        // Removes empty record at the end of the array
        $arDfCommandResultNoDoubleSpaces = array_filter($arDfCommandResultNoDoubleSpaces);

        function aasort (&$array, $key) {
            $sorter = array();
            reset($array); // Rewinds array's internal pointer to the first element

            foreach ($array as $uKey => $uValue) {
                $sorter[$uKey] = intval($uValue[$key]);
            }

            arsort($sorter);

            foreach ($sorter as $element => $value) {
                $ret[] = $array[$element];
            }

            $array = $ret;
        }

        aasort($arDfCommandResultNoDoubleSpaces, '3');

        $topPercent = (integer) $arDfCommandResultNoDoubleSpaces[0][3];
        $this->topPercent = $topPercent;

        $arHeaderResultNoDoubleSpaces[0] = 'Drive:';
        $arHeaderResultNoDoubleSpaces[1] = 'Used:';
        $arHeaderResultNoDoubleSpaces[2] = 'Avail:';
        $arHeaderResultNoDoubleSpaces[3] = '% Full:';

        array_unshift($arDfCommandResultNoDoubleSpaces , $arHeaderResultNoDoubleSpaces);

        foreach($arDfCommandResultNoDoubleSpaces as $key => $tableRow) {
            if($key == 0) {
                $tableString = "<table width='350' cellpadding='7' border='1' cellspacing='0'>
                                    <tr>
                                        <th>$tableRow[0]</th>
                                        <th>$tableRow[1]</th>
                                        <th>$tableRow[2]</th>
                                        <th>$tableRow[3]</th>
                                    </tr>";
            } else {
                $percent = (integer) $tableRow['3'];
                $yellow = $this->checkIfYellow($percent);

                if($yellow) {
                    $tableString .= "	<tr style='background-color: yellow'>";
                } else {
                    $tableString .= "	<tr>";
                }

                $tableString .= 	"<td style='padding-left: 25px;'>$tableRow[0]</td>
                                        <td>$tableRow[1]</td>
                                        <td>$tableRow[2]</td>
                                        <td>$tableRow[3]</td>
                                    </tr>";
            }
        }
        $tableString .= 		"</table>";

        return array('tableString' => $tableString, 'topPercent' => $topPercent);
    }

    /**
     * sends email
     *
     * @param string $emailAddresses
     * @param string $subject
     * @param string $message
     *
     * @return void
     */	
    public function sendMessage($from_address, $emailAddresses, $subject, $message) {
        $headers = 	'From: ' . $from_address . "\r\n" .
                    'Reply-To: ' . $from_address . "\r\n" .
                    'X-Mailer: PHP/' . phpversion() . "\r\n" .
                    'Content-type:text/html;charset=UTF-8 \r\n \r\n';

        $message .= "</br>The Magento 2 Disk Space Report can help make sure that your site doesn't 
                    run out of disk space.  Running out of disk space means your site will shut 
                    down, and your database may be corrupted. </br></br>";

        $message .= "This report was auto-magically generated from your Magento 2 site. You can 
                    control the settings from your Magento admin area (such as changing the frequency 
                    or email address). </br></br>";

        $message .= "Need help with your Magento 2 site?  Customer Paradigm can help!  Contact Customer 
                    Paradigm at 303.473.4400 or visit <a href='http://www.customerparadigm.com'>
                    http://www.customerparadigm.com</a> </br></br>";

        // if email addresses exist
        if ($emailAddresses) {
            // get email addresses in array
            $emailArray = explode (',', $emailAddresses);
            // format message
            // $message = wordwrap($message, 500, "</br>");
            // send email to each recipient
            foreach ($emailArray as $email) {
                // send email
                mail($email, $subject, $message, $headers);
            }
        }
    }

    public function checkIfYellow($percent) {
        $emailCollection = $this->emailFactory->create()->getCollection();
        if ($emailModel = $emailCollection->getFirstItem()) {
            $sendEmailSetting = $emailModel->getData('send_email');
        }

        if(
            ($sendEmailSetting == '0' && $percent >= 80) ||
            ($sendEmailSetting == '1' && $percent >= 60) ||
            ($sendEmailSetting == '2' && $percent >= 65) ||
            ($sendEmailSetting == '3' && $percent >= 70) ||
            ($sendEmailSetting == '4' && $percent >= 75) ||
            ($sendEmailSetting == '5' && $percent >= 80) ||
            ($sendEmailSetting == '6' && $percent >= 85) ||
            ($sendEmailSetting == '7' && $percent >= 90) ||
            ($sendEmailSetting == '8' && $percent >= 95) ||
            ($sendEmailSetting == '9' && $percent >= 99)
        ) {
            return true;
        } else {
            return false;
        }
    }
}