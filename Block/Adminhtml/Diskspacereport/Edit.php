<?php
/**
 * Copyright © 2015 Customer Paradigm. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace CustomerParadigm\DiskSpaceReport\Block\Adminhtml\Diskspacereport;

use \Magento\Backend\Block\Widget\Form\Container;

class Edit extends Container
{
    /**
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }

    protected function _construct()
    {
        $this->_objectId = 'id';
        $this->_blockGroup = 'CustomerParadigm_DiskSpaceReport';
        $this->_controller = 'adminhtml_diskspacereport';

        parent::_construct();
    }

    /**
     * Retrieve text for header element depending on loaded news
     * 
     * @return string
     */
    public function getHeaderText()
    {
        return __('Disk Space Report Settings');
    }

    /**
     * Prepare grid / button(s)
     *
     * @return $this
     */
    protected function _prepareLayout()
    {	
        $this->buttonList->remove('back');
        $this->buttonList->remove('reset');
        return parent::_prepareLayout();
    }

}