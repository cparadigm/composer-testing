<?php
/**
 * Copyright © 2015 Customer Paradigm. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace CustomerParadigm\DiskSpaceReport\Controller\Adminhtml\DiskSpaceReport;

use Magento\Backend\App\Action;

class Index extends Action
{
    /** @var PageFactory */
    protected $resultPageFactory;

    /**
     * @param Magento\Backend\App\Action\Context $context
     * @param Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('CustomerParadigm_DiskSpaceReport::diskspacereport');
    }

    /**
     * Index action
     *
     * @return void
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('CustomerParadigm_DiskSpaceReport::diskspacereport');
        $resultPage->getConfig()->getTitle()->prepend(__('Disk Space Report Settings'));

        return $resultPage; 
    }
}
