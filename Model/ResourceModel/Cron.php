<?php
/**
 * Copyright © 2015 Customer Paradigm. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace CustomerParadigm\DiskSpaceReport\Model\ResourceModel;

class Cron extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     * @param string|null $resourcePrefix
     */
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        $resourcePrefix = null
    )
    {
        $this->transactionManager = $context->getTransactionManager();
        $this->_resources = $context->getResources();
        $this->objectRelationProcessor = $context->getObjectRelationProcessor();
        if ($resourcePrefix !== null) {
            $this->_resourcePrefix = $resourcePrefix;
        }
        parent::__construct($context);
    }	

    protected function _construct()
    {
        $this->_init(
            'cp_diskspacereport_cron',
            'id'
        );
    }

}