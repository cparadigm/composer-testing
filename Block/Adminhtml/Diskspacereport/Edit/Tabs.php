<?php
/**
 * Copyright © 2015 Customer Paradigm. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace CustomerParadigm\DiskSpaceReport\Block\Adminhtml\Diskspacereport\Edit;
 
use Magento\Backend\Block\Widget\Tabs as WidgetTabs;
 
class Tabs extends WidgetTabs
{
    /** @var \Magento\Framework\Registry */
    protected $_coreRegistry = null;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Json\EncoderInterface $jsonEncoder
     * @param \Magento\Backend\Model\Auth\Session $authSession
     * @param \Magento\Framework\Module\Manager $moduleManager
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Magento\Backend\Model\Auth\Session $authSession,
        \Magento\Framework\Module\Manager $moduleManager,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        $this->_coreRegistry = $registry;
        parent::__construct($context, $jsonEncoder, $authSession, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('diskspacereport_edit_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Disk Space Report Settings'));
    }
 
    /**
     * @return $this
     */
    protected function _beforeToHtml()
    {
        // cron settings tab
        $this->addTab(
            'diskspacereport_edit_settings',
            [
                'label' => __('Manage CRON Settings'),
                'title' => __('Manage CRON Settings'),
                'content' => $this->getLayout()->createBlock(
                    'CustomerParadigm\DiskSpaceReport\Block\Adminhtml\Diskspacereport\Edit\Tab\Settings')->toHtml()
            ]
        );
        // email settings tab
        $this->addTab(
            'diskspacereport_edit_email',
            [
                'label' => __('Manage Email Settings'),
                'title' => __('Manage Email Settings'),
                'content' => $this->getLayout()->createBlock(
                    'CustomerParadigm\DiskSpaceReport\Block\Adminhtml\Diskspacereport\Edit\Tab\Email')->toHtml()
            ]
        );

        return parent::_beforeToHtml();
    }
}