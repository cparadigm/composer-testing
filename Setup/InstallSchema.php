<?php
/**
 * Copyright © 2015 Customer Paradigm. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace CustomerParadigm\DiskSpaceReport\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        /**
         * Create table 'cp_diskspacereport_cron'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('cp_diskspacereport_cron')
            )->addColumn(
                'id',
                \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                null,
                ['identity' => true, 'nullable' => false, 'primary' => true],
                'Id'
            )->addColumn(
                'status',
                \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
                null,
                ['nullable' => false, 'default' => 0],
                'Status'
            )->addColumn(
                'minute',
                \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                null,
                ['nullable' => true],
                'Minute'
            )->addColumn(
                'hour',
                \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                null,
                ['nullable' => true],
                'Hour'
            )->addColumn(
                'dmonth',
                \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                null,
                ['nullable' => true],
                'Day of Month'
            )->addColumn(
                'month',
                \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                null,
                ['nullable' => true],
                'Month'
            )->addColumn(
                'dweek',
                \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                null,
                ['nullable' => true],
                'Day of Week'
            )->addIndex(
                $installer->getIdxName('id', ['id']),
                ['id']
        );
        $installer->getConnection()->createTable($table);

        /**
         * Create table 'cp_diskspacereport_email'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('cp_diskspacereport_email')
            )->addColumn(
                'email_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                null,
                ['identity' => true, 'nullable' => false, 'primary' => true],
                'Id'
            )->addColumn(
                'settings_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                null,
                ['nullable' => false],
                'Account Id'
            )->addColumn(
                'from_address',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                1024,
                ['nullable' => true],
                'From Email Address'
            )->addColumn(
                'addresses',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                1024,
                ['nullable' => true],
                'Email Addresses'
            )->addColumn(
                'subject',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                156,
                ['nullable' => true],
                'Email Subject Line'
            )->addColumn(
                'send_email',
                \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
                null,
                ['nullable' => false, 'default' => 0],
                'Send Email'
            )->addIndex(
                $installer->getIdxName('email_id', ['email_id']),
                ['email_id']
            )->addForeignKey(
                $installer->getFkName(
                    'cp_diskspacereport_email',
                    'settings_id',
                    'cp_diskspacereport_cron',
                    'id'
                ),
                'settings_id',
                $installer->getTable('cp_diskspacereport_cron'),
                'id',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        );
        $installer->getConnection()->createTable($table);	

        $installer->endSetup();
    }
}