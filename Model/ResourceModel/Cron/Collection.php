<?php
/**
 * Copyright © 2015 Customer Paradigm. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace CustomerParadigm\DiskSpaceReport\Model\ResourceModel\Cron;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init(
            'CustomerParadigm\DiskSpaceReport\Model\Cron',
            'CustomerParadigm\DiskSpaceReport\Model\ResourceModel\Cron'
        );
    }
}