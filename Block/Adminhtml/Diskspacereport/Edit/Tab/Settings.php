<?php
/**
 * Copyright © 2015 Customer Paradigm. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace CustomerParadigm\DiskSpaceReport\Block\Adminhtml\Diskspacereport\Edit\Tab;

use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Widget\Tab\TabInterface;

class Settings extends Generic implements TabInterface
{
    /** @var \CustomerParadigm\DiskSpaceReport\Model\CronFactory */
    protected $cronFactory;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \CustomerParadigm\DiskSpaceReport\Model\CronFactory $cronFactory,
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \CustomerParadigm\DiskSpaceReport\Model\CronFactory $cronFactory,
        array $data = []
    ) {
        parent::__construct($context, $registry, $formFactory, $data);
        $this->cronFactory = $cronFactory;
        $this->setUseContainer(true);
    }

    /**
     * Init form
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('edit_form');
        $this->setTitle(__('CRON Settings'));
    }

    /**
     * Prepare form
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        /** @var \CustomerParadigm\DiskSpaceReport\Model\Resource\Cron\Collection */
        $collection = $this->cronFactory->create()->getCollection();
        /** @var \CustomerParadigm\DiskSpaceReport\Model\Cron */
        $model = $collection->getFirstItem();

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create(
            ['data' => ['id' => 'edit_form', 'action' => $this->getData('action'), 'method' => 'post']]
        );

        $fieldset = $form->addFieldset(
            'edit_form_fieldset_selling',
            ['legend' => __('CRON Settings')]
        );
        // id field
        $id = $fieldset->addField(
            'id',
            'hidden',
            [
                'name' => 'id',
                'value' => $model->getData('id'),
            ]
        );
        // set message
        $id->setAfterElementHtml('
            <div class="messages">
                <div class="message message-notice">
                    Choose whether to enable / disable the Disk Space Check CRON run.  If enabled, please enter the frequency with which to be executed.  <br><br>For further assistance with your Magento site, please visit Customer Paradigm at <a href="http://www.customerparadigm.com">www.customerparadigm.com</a> or 303.473-4400.
                </div>
            </div>		
        ');
        //status field
        $statusField = $fieldset->addField(
            'status',
            'select',
            [
                'name' => 'status',
                'label' => __('Status'),
                'title' => __('Status'),
                'value' => $model->getData('status'),
                'required' => true,
                'options' => [
                    '0' => __('Disabled'),
                    '1' => __('Enabled')
                ]
            ]
        );

        /** @var int $minute */
        $minute = '98';
        if ($model->getMinute()) {
            $minute = $model->getMinute();
        }
        elseif ($model->getMinute() === '0') {
            $minute = '0';
        }
        // minute field
        $minuteField = $fieldset->addField(
            'minute',
            'select',
            [
                'label' => __('Minute'),
                'title' => __('Minute'),
                'name' => 'minute',
                'value' => $minute,
                'options' => [
                    '98' => __('Not Selected'),
                    '99' => __('Every 15 Minutes'),
                    '0' => __('Start of Each Hour'),
                    '15' => __('15 Minutes After Each Hour'),
                    '30' => __('30 Minutes After Each Hour'),
                    '45' => __('45 Minutes After Each Hour')
                ]
            ]
        );

        /** @var int $hour */
        $hour = '98';
        if ($model->getHour()) {
            $hour = $model->getHour();
        }
        elseif ($model->getHour() === '0') {
            $hour = '0';
        }
        // hour field
        $hourField = $fieldset->addField(
            'hour',
            'select',
            [
                'label' => __('Hour'),
                'title' => __('Hour'),
                'name' => 'hour',
                'value' => $hour,
                'options' => [
                    '98' => __('Not Selected'),
                    '99' => __('Every Hour'),
                    '0' => __('Midnight'),
                    '1' => __('1AM'),
                    '2' => __('2AM'),
                    '3' => __('3AM'),
                    '4' => __('4AM'),
                    '5' => __('5AM'),
                    '6' => __('6AM'),
                    '7' => __('7AM'),
                    '8' => __('8AM'),
                    '9' => __('9AM'),
                    '10' => __('10AM'),
                    '11' => __('11AM'),
                    '12' => __('Noon'),
                    '13' => __('1PM'),
                    '14' => __('2PM'),
                    '15' => __('3PM'),
                    '16' => __('4PM'),
                    '17' => __('5PM'),
                    '18' => __('6PM'),
                    '19' => __('7PM'),
                    '20' => __('8PM'),
                    '21' => __('9PM'),
                    '22' => __('10PM'),
                    '23' => __('11PM')
                ]
            ]
        );

        /** @var int $dMonth */
        $dMonth = '98';
        if ($model->getDmonth()) {
            $dMonth = $model->getDmonth();
        }
        elseif ($model->getDmonth() === '0') {
            $dMonth = '0';
        }

        /** @var array[] $options */
        $options = array();
        $options[98] = 'Not Selected';
        $options[99] = 'Every Day of the Month';
        for ($i = 1; $i < 32; $i++) {
            $options[$i] = $i;
        }
        // dmonth field
        $dmonthField = $fieldset->addField(
            'dmonth',
            'select',
            [
                'label' => __('Day Of Month'),
                'title' => __('Day of Month'),
                'name' => 'dmonth',
                'value' => $dMonth,
                'options' => $options
            ]
        );

        /** @var int $month */
        $month = '98';
        if ($model->getMonth()) {
            $month = $model->getMonth();
        }
        elseif ($model->getMonth() === '0') {
            $month = '0';
        }
        // month field
        $monthField = $fieldset->addField(
            'month',
            'select',
            [
                'label' => __('Month'),
                'title' => __('Month'),
                'name' => 'month',
                'value' => $month,
                'options' => [
                    '98' => __('Not Selected'),
                    '99' => __('Every Month'),
                    '1' => __('January'),
                    '2' => __('February'),
                    '3' => __('March'),
                    '4' => __('April'),
                    '5' => __('May'),
                    '6' => __('June'),
                    '7' => __('July'),
                    '8' => __('Augest'),
                    '9' => __('September'),
                    '10' => __('October'),
                    '11' => __('November'),
                    '12' => __('December')
                ]
            ]
        );

        /** int $dWeek */
        $dWeek = '98';
        if ($model->getDweek()) {
            $dWeek = $model->getDweek();
        }
        elseif ($model->getDweek() === '0') {
            $dWeek = '0';
        }
        // dweek field
        $dweekField = $fieldset->addField(
            'dweek',
            'select',
            [
                'label' => __('Day of Week'),
                'title' => __('Day of Week'),
                'name' => 'dweek',
                'value' => $dWeek,
                'options' => [
                    '98' => __('Not Selected'),
                    '99' => __('Every Day'),
                    '1' => __('Monday'),
                    '2' => __('Tuesday'),
                    '3' => __('Wednesday'),
                    '4' => __('Thursday'),
                    '5' => __('Friday'),
                    '6' => __('Saturday'),
                    '7' => __('Sunday')
                ]
            ]
        );

        $notes = $fieldset->addField(
            'note',
            'note',
            array(
                'text' => __('<span style="color:red;">For CRON to be enabled, you must make a selection for each of the CRON time settings.</span>')
            )
        );

        $this->setForm($form);
        // set form dependencies
        $this->setChild('form_after',
            $this->getLayout()->createBlock(
                'Magento\Backend\Block\Widget\Form\Element\Dependence')
            ->addFieldMap(
                $statusField->getHtmlId(),
                $statusField->getName()
            )->addFieldMap(
                $minuteField->getHtmlId(),
                $minuteField->getName()
            )->addFieldMap(
                $hourField->getHtmlId(),
                $hourField->getName()
            )->addFieldMap(
                $dmonthField->getHtmlId(),
                $dmonthField->getName()
            )->addFieldMap(
                $monthField->getHtmlId(),
                $monthField->getName()
            )->addFieldMap(
                $dweekField->getHtmlId(),
                $dweekField->getName()
            )->addFieldMap(
                $notes->getHtmlId(),
                $notes->getName()
            )->addFieldDependence(
                $minuteField->getName(),
                $statusField->getName(),
                '1'	
            )->addFieldDependence(
                $hourField->getName(),
                $statusField->getName(),
                '1'
            )->addFieldDependence(
                $dmonthField->getName(),
                $statusField->getName(),
                '1'
            )->addFieldDependence(
                $monthField->getName(),
                $statusField->getName(),
                '1'
            )->addFieldDependence(
                $dweekField->getName(),
                $statusField->getName(),
                '1'
            )->addFieldDependence(
                $notes->getName(),
                $statusField->getName(),
                '1'
            ));
        return parent::_prepareForm();		
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return __('CRON Settings');
    }
 
    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return __('CRON Settings');
    }
 
    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }
 
    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

}

