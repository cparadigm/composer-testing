<?php
/**
 * Copyright © 2015 Customer Paradigm. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace CustomerParadigm\DiskSpaceReport\Block\Adminhtml\Diskspacereport\Edit\Tab;

use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Widget\Tab\TabInterface;

class Email extends Generic implements TabInterface
{
    /** @var \CustomerParadigm\DiskSpaceReport\Model\EmailFactory */
    protected $emailFactory;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \CustomerParadigm\DiskSpaceReport\Model\EmailFactory $emailFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \CustomerParadigm\DiskSpaceReport\Model\EmailFactory $emailFactory,
        array $data = []
    ) {
        parent::__construct($context, $registry, $formFactory, $data);
        $this->emailFactory = $emailFactory;
        $this->setUseContainer(true);
    }

    /**
     * Init form
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('edit_form');
        $this->setTitle(__('Email Settings'));
    }

    /**
     * Prepare form
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        /** @var \CustomerParadigm\DiskSpaceReport\Model\Resource\Email\Collection */
        $collection = $this->emailFactory->create()->getCollection();
        /** @var \CustomerParadigm\DiskSpaceReport\Model\Email */
        $model = $collection->getFirstItem();

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create(
            ['data' => ['id' => 'edit_form', 'action' => $this->getData('action'), 'method' => 'post']]
        );

        $fieldset = $form->addFieldset(
            'edit_form_fieldset_selling',
            ['legend' => __('Email Settings')]
        );
        // email_id field
        $emailId = $fieldset->addField(
            'email_id',
            'hidden',
            [
                'name' => 'email_id',
                'value' => $model->getData('email_id'),
            ]
        );
        // set header message
        $emailId->setAfterElementHtml('
            <div class="messages">
                <div class="message message-notice">
                    Please choose the email settings.  These settings will be used to send results of the Disk Space Report CRON results.  <br><br>For further assistance with your Magento site, please visit Customer Paradigm at <a href="http://www.customerparadigm.com">www.customerparadigm.com</a> or 303.473-4400.
                </div>
            </div>		
        ');
        // addresses field
        $from_address = $fieldset->addField(
            'from_address',
            'text',
            [
                'name' => 'from_address',
                'label' => __('Send From Email Address'),
                'title' => __('Send From Email Address'),
                'value' => $model->getData('from_address'),
                'required' => true,
                'note' => __('Please indicate the email address to send the email messages from.  For best deliverability, we recommend using an email address that is associated with the domain of your Magento site (i.e. a gmail or yahoo email address may not work).')
            ]
        );
        // addresses field
        $addresses = $fieldset->addField(
            'addresses',
            'text',
            [
                'name' => 'addresses',
                'label' => __('Email Addresses'),
                'title' => __('Email Addresses'),
                'value' => $model->getData('addresses'),
                'required' => true,
                'note' => __('For multiple email addresses, please separate each address using a comma (ex:  emailone@example.com,emailtwo@example.com)')
            ]
        );
        // subject field
        $subject = $fieldset->addField(
            'subject',
            'text',
            [
                'name' => 'subject',
                'label' => __('Subject Line'),
                'title' => __('Subject Line'),
                'value' => (!empty($model->getData('subject')))?$model->getData('subject'):'Magento 2 Disk Space Report',
                'required' => true,
                'note' => __('Used to personalize the subject line the email results.')
            ]
        );
        // email field
        $email = $fieldset->addField(
            'send_email',
            'select',
            [
                'name' => 'send_email',
                'label' => __('Email Report When'),
                'title' => __('Email Report When'),
                'value' => $model->getData('send_email'),
                'required' => true,
                'options' => [
                    '0' => __('Always Send Email'),
                    '1' => __('When drive is above 60%'),
                    '2' => __('When drive is above 65%'),
                    '3' => __('When drive is above 70%'),
                    '4' => __('When drive is above 75%'),
                    '5' => __('When drive is above 80%'),
                    '6' => __('When drive is above 85%'),
                    '7' => __('When drive is above 90%'),
                    '8' => __('When drive is above 95%'),
                    '9' => __('When drive is above 99%')
                ]
            ]
        );

        $this->setForm($form);

        return parent::_prepareForm();		
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return __('Email Settings');
    }
 
    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return __('Email Settings');
    }
 
    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }
 
    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

}

