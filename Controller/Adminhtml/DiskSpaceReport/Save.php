<?php
/**
 * Copyright © 2015 Customer Paradigm. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace CustomerParadigm\DiskSpaceReport\Controller\Adminhtml\DiskSpaceReport;
 
use Magento\Backend\App\Action;
 
class Save extends Action
{	
    /** @var \CustomerParadigm\DiskSpaceReport\Model\CronFactory */
    protected $cronFactory;
    /** @var \CustomerParadigm\DiskSpaceReport\Model\EmailFactory */
    protected $emailFactory;

    /**
     * @param Action\Context $context
     * @param CustomerParadigm\DiskSpaceReport\Model\CronFactory $cronFactory
     * @param CustomerParadigm\DiskSpaceReport\Model\EmailFacotry $emailFactory
     */
    public function __construct(
        Action\Context $context,
        \CustomerParadigm\DiskSpaceReport\Model\CronFactory $cronFactory,
        \CustomerParadigm\DiskSpaceReport\Model\EmailFactory $emailFactory
    )
    {
        parent::__construct($context);
        $this->cronFactory = $cronFactory;
        $this->emailFactory = $emailFactory;
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('CustomerParadigm_DiskSpaceReport::diskspacereport');
    }

   /**
    * @return void
    */
   public function execute()
   {   
        /** @var \CustomerParadigm\DiskSpaceReport\Model\Resource\Cron\Collection */
        $cronCollection = $this->cronFactory->create()->getCollection();
        /** @var \CustomerParadigm\DiskSpaceReport\Model\Cron */
        $cronModel = $cronCollection->getFirstItem();
        /** @var \CustomerParadigm\DiskSpaceReport\Model\Resource\Email\Collection */
        $emailCollection = $this->emailFactory->create()->getCollection();
        /** @var \CustomerParadigm\DiskSpaceReport\Model\Email */
        $emailModel = $emailCollection->getFirstItem();
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        // verify cron model exists, if not, create
        if (! $cronModel->getId()) {
            $cronModel = $this->cronFactory->create();
        }
        if (! $emailModel->getEmailId()) {
            $emailModel = $this->emailFactory->create();
        }

        // sets the cron model
        if ($status = $this->getRequest()->getParam('status')) {
            $cronModel->setData('status', $status);
        }
        else {
            $cronModel->setData('status', 0);
        }
        if ($minute = $this->getRequest()->getParam('minute')) {
            $cronModel->setData('minute', $minute);
        }
        else {
            if ($minute === '0') {
                $cronModel->setData('minute', 0);
            }
            else {
                $cronModel->setData('minute', 98);
            }
        }
        if ($hour = $this->getRequest()->getParam('hour')) {
            $cronModel->setData('hour', $hour);
        }
        else {
            if ($hour === '0') {
                $cronModel->setData('hour', 0);
            }
            else {
                $cronModel->setData('hour', 98);
            }
        }
        if ($dmonth = $this->getRequest()->getParam('dmonth')) {
            $cronModel->setData('dmonth', $dmonth);
        }
        else {
            if ($dmonth === '0') {
                $cronModel->setData('dmonth', 0);
            }
            else {
                $cronModel->setData('dmonth', 98);
            }
        }
        if ($month = $this->getRequest()->getParam('month')) {
            $cronModel->setData('month', $month);
        }
        else {
            if ($month === '0') {
                $cronModel->setData('month', 0);
            }
            else {
                $cronModel->setData('month', 98);
            }
        }
        if ($dweek = $this->getRequest()->getParam('dweek')) {
            $cronModel->setData('dweek', $dweek);
        }
        else {
            if ($dweek === '0') {
                $cronModel->setData('dweek', 0);
            }
            else {
                $cronModel->setData('dweek', 98);
            }
        }

        // sets the email model
        if ($from_address = $this->getRequest()->getParam('from_address')) {
            $emailModel->setData('from_address', $from_address);
        }
        if ($addresses = $this->getRequest()->getParam('addresses')) {
            $emailModel->setData('addresses', $addresses);
        }
        if ($subject = $this->getRequest()->getParam('subject')) {
            $emailModel->setData('subject', $subject);
        }
        if ($sendEmail = $this->getRequest()->getParam('send_email')) {
            $emailModel->setData('send_email', $sendEmail);
        }
        else {
            if ($sendEmail === '0') {
                $emailModel->setData('send_email', 0);
            }
            else {
                $emailModel->setData('send_email', 1);
            }
        }

        // save the submitted form data
        try {
            $cronModel->save();
            if (! $emailModel->getData('settings_id')) {
                $emailModel->setData('settings_id', $cronModel->getData('id'));
            }
            $emailModel->save();
        }
        catch (Exception $e) {
            $this->messageManager
                ->addError(__('There was an error saving the information. Please try again.'));
            return $resultRedirect->setPath('diskspacereport/diskspacereport/index');
        }

        // redirect after saving
        $this->messageManager
            ->addSuccess(__('You have successfully saved the information.'));
        return $resultRedirect->setPath('diskspacereport/diskspacereport/index');
   }
}